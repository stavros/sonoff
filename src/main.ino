#include <ESP8266WiFi.h>
#include <EEPROM.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <WiFiUdp.h>
#include <PubSubClient.h>
#include <ESP8266mDNS.h>
#include <ArduinoOTA.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>

#define RELAY_PIN 12
#define GLED_PIN 13

#define BTN_PIN 0
#define BTN_PRESSED 0
#define BTN_UNPRESSED 1

#define LED_ON LOW
#define LED_OFF HIGH

#define RELAY_OFF 0
#define RELAY_ON 1

#ifndef VERSION
#define VERSION "1"
#endif

#ifndef PROJECT_NAME
#define PROJECT_NAME "project"
#endif

#define MQTT_PORT 1883

#define CONFIG_VERSION "b03"

char GROUP_TOPIC[128];
char COMMAND_TOPIC[128];
char STATE_TOPIC[128];
char LOG_TOPIC[128];

char INSTANCE_NAME[16];

WiFiClient wclient;
PubSubClient mqttClient(wclient);

struct State {
    char button = BTN_UNPRESSED;
    unsigned long lastPress = 0;
    unsigned long lastMQTTTry = 0;
    unsigned long lastWiFiTry = 0;
    unsigned long lastAutoOn = 0;
    unsigned long lastAutoOff = 0;
    char relay = RELAY_OFF;
    bool shouldSave = false;
} state;

struct Persistent {
    char version[4] = CONFIG_VERSION;
    char defaultState = 0;
    unsigned int turnOffAfter = 0;
    unsigned int turnOnAfter = 0;
    char mqttServer[64] = "";
} persistent;


void loadState() {
    EEPROM.begin(sizeof(persistent));
    if (EEPROM.read(0) != CONFIG_VERSION[0] ||
        EEPROM.read(1) != CONFIG_VERSION[1] ||
        EEPROM.read(2) != CONFIG_VERSION[2]) {
        strcpy(persistent.version, "\0\0\0");
        return;
    }

    for (unsigned int t=0; t<sizeof(persistent); t++)
        *((char*)&persistent + t) = EEPROM.read(t);
    EEPROM.end();

    Serial.println("Loaded state.");
}


void saveState() {
    strcpy(persistent.version, CONFIG_VERSION);

    EEPROM.begin(sizeof(persistent));
    for (unsigned int t=0; t<sizeof(persistent); t++)
        EEPROM.write(t, *((char*)&persistent + t));
    EEPROM.end();
    Serial.println("Saved state.");
}

void saveStateCallback () {
    Serial.println("Should save state.");
    state.shouldSave = true;
}


void mqttPublish(String topic, String payload) {
    char chPayload[500];
    char chTopic[100];

    if (!mqttClient.connected()) {
        return;
    }

    topic.toCharArray(chTopic, 100);
    ("(" + String(millis()) + " - " + WiFi.localIP().toString() + ") " + INSTANCE_NAME + ": " + payload).toCharArray(chPayload, 100);
    mqttClient.publish(chTopic, chPayload);
}


void logUDP(String message) {
#ifdef UDP_LOG_IP
    WiFiUDP Udp;

    // Listen with `nc -ul 37243`.
    Udp.beginPacket(UDP_LOG_IP, 37243);
    Udp.write(("(" + String(millis()) + " - " + WiFi.localIP().toString() + ") " + PROJECT_NAME + ": " + message).c_str());
    Udp.endPacket();
#endif
}


void connectMQTT() {
    if (mqttClient.connected()) {
        mqttClient.loop();
        return;
    }

    if (state.lastMQTTTry > millis() - 60000) return;

    state.lastMQTTTry = millis();

    mqttClient.setServer(persistent.mqttServer, MQTT_PORT);
    mqttClient.setCallback(mqttCallback);

    int retries = 2;
    logUDP("\nConnecting to MQTT...");
    while (!mqttClient.connect(INSTANCE_NAME) && retries--) {
        delay(500);
        logUDP("Retry...");
    }

    if (!mqttClient.connected()) {
        logUDP("\nfatal: MQTT server connection failed.");
    } else {
        mqttClient.subscribe(COMMAND_TOPIC);
        mqttClient.subscribe(GROUP_TOPIC);

        logUDP("Connected.");
        mqttPublish(LOG_TOPIC, String("Connected."));
    }
}


// Receive a message from MQTT and act on it.
void mqttCallback(char* chTopic, byte* chPayload, unsigned int length) {
    chPayload[length] = '\0';
    parseCommand((char *)chPayload);
}


#ifdef OTA_SERVER
void doHTTPUpdate() {
    if (WiFi.status() != WL_CONNECTED) return;

    t_httpUpdate_return ret = ESPhttpUpdate.update("http://" OTA_SERVER "/" PROJECT_NAME "/", VERSION);

    switch(ret) {
        case HTTP_UPDATE_FAILED:
            Serial.printf("HTTP_UPDATE_FAILED Error (%d): %s\r\n", ESPhttpUpdate.getLastError(), ESPhttpUpdate.getLastErrorString().c_str());
            break;

        case HTTP_UPDATE_NO_UPDATES:
            Serial.println("HTTP_UPDATE_NO_UPDATES");
            break;

        case HTTP_UPDATE_OK:
            Serial.println("HTTP_UPDATE_OK");
            break;
    }
}
#endif


void connectWifi() {
    WiFiManager wifiManager;
    char apName[64];

    snprintf(apName, 64, PROJECT_NAME "-%08X", ESP.getChipId());

    if (persistent.version[0] == 0) wifiManager.resetSettings();

    WiFiManagerParameter defaultState("default_state", "Default switch state (0/1)", "0", 30);
    wifiManager.addParameter(&defaultState);

    WiFiManagerParameter turnOffAfter("turn_off_after", "Turn off after X seconds", "", 4);
    wifiManager.addParameter(&turnOffAfter);

    WiFiManagerParameter turnOnAfter("turn_on_after", "Turn on after X seconds", "", 4);
    wifiManager.addParameter(&turnOnAfter);

    WiFiManagerParameter mqttServer("mqtt_server", "MQTT server hostname", "", 63);
    wifiManager.addParameter(&mqttServer);

    WiFi.hostname((String("SON_") + String(INSTANCE_NAME)).c_str());

    wifiManager.setConfigPortalTimeout(180);
    wifiManager.setSaveConfigCallback(saveStateCallback);

    if (!wifiManager.autoConnect(apName)) {
        Serial.println("Failed to connect.");
        ESP.reset();
        delay(5000);
    }

    if (state.shouldSave) {
        // An MQTT server was specified.
        strncpy(persistent.mqttServer, mqttServer.getValue(), 30);

        if (defaultState.getValue()[0] == '1')
            persistent.defaultState = 1;
        else
            persistent.defaultState = 0;

        persistent.turnOffAfter = atoi(turnOffAfter.getValue());

        persistent.turnOnAfter = atoi(turnOnAfter.getValue());

        Serial.print("Configured. MQTT server name: ");
        Serial.println(persistent.mqttServer);

        Serial.print("Configured. Default state: ");
        Serial.println(persistent.defaultState, DEC);

        saveState();
        state.shouldSave = false;
    }

    Serial.println("Connected to WiFi. IP:");
    Serial.println(WiFi.localIP());
}


void parseCommand(char* cmd) {
    WiFiManager wifiManager;
    String command = String((char*)cmd);

    Serial.println(String("Command ") + command + String(" received."));

    if (command == "power on" || command == "relay on") {
        relayOn();
    } else if (command == "power off" || command == "relay off") {
        relayOff();
    } else if (command == "power toggle" || command == "relay toggle") {
        relayToggle();
    } else if (command == "status" || command == "state") {
        publishState();
    } else if (command == "nuke") {
        wifiManager.resetSettings();
    } else if (command == "reboot") {
        ESP.reset();
    }
}


void publishState() {
    if (state.relay == RELAY_ON) {
        Serial.println("Power on.");
        mqttPublish(STATE_TOPIC, "power on");
    } else {
        Serial.println("Power off.");
        mqttPublish(STATE_TOPIC, "power off");
    }
}


void changeAfter(unsigned int duration, char newState) {
    unsigned long currentSecs = millis() / 1000;
    unsigned long newTime = currentSecs + duration;

    if (duration <= 0) return;

    if (newState == 0) {
        state.lastAutoOff = newTime;
    } else {
        state.lastAutoOn = newTime;
    }
}


void relayOn() {
    digitalWrite(RELAY_PIN, RELAY_ON);
    state.relay = RELAY_ON;
    digitalWrite(GLED_PIN, LED_ON);
    publishState();
    changeAfter(persistent.turnOffAfter, 0);
}


void relayOff() {
    digitalWrite(RELAY_PIN, RELAY_OFF);
    state.relay = RELAY_OFF;
    digitalWrite(GLED_PIN, LED_OFF);
    publishState();
    changeAfter(persistent.turnOnAfter, 1);
}


void relayToggle() {
    if (state.relay == RELAY_OFF) {
        relayOn();
    } else {
        relayOff();
    }
}


void checkState() {
    // Check whether we need to turn the switch on or off at some point.

    if (state.lastAutoOn > 0 and state.lastAutoOn < (millis() / 1000)) {
        relayOn();
        state.lastAutoOn = 0;
    }

    if (state.lastAutoOff > 0 and state.lastAutoOff < (millis() / 1000)) {
        relayOff();
        state.lastAutoOff = 0;
    }
}

void checkButton() {
    char btn = digitalRead(BTN_PIN);

    if (btn == state.button || (state.lastPress > millis() - 50)) return;

    if (btn == BTN_PRESSED) {
        relayToggle();
    }

    state.button = btn;
    state.lastPress = millis();
}


void resetPins() {
    pinMode(1, OUTPUT);
    analogWrite(1, 0);
    pinMode(2, OUTPUT);
    analogWrite(2, 0);
    pinMode(3, OUTPUT);
    analogWrite(3, 0);
    pinMode(4, OUTPUT);
    analogWrite(4, 0);
    pinMode(5, OUTPUT);
    analogWrite(5, 0);
    pinMode(12, OUTPUT);
    analogWrite(12, 0);
    pinMode(13, OUTPUT);
    analogWrite(13, 0);
    pinMode(14, OUTPUT);
    analogWrite(14, 0);
    pinMode(15, OUTPUT);
    analogWrite(15, 0);
}


void setup() {
    resetPins();

    pinMode(RELAY_PIN, OUTPUT);
    pinMode(GLED_PIN, OUTPUT);
    pinMode(BTN_PIN, INPUT);

    analogWrite(RELAY_PIN, RELAY_OFF);
    digitalWrite(GLED_PIN, LED_ON);

    Serial.begin(115200);
    Serial.println("\n\nBooting version " VERSION "...");

    loadState();

    if (persistent.defaultState == 1)
        relayOn();
    else
        relayOff();

    snprintf(GROUP_TOPIC, 128, PROJECT_NAME "/all/command");
    snprintf(COMMAND_TOPIC, 128, PROJECT_NAME "/%08X/command", ESP.getChipId());
    snprintf(STATE_TOPIC, 128, PROJECT_NAME "/%08X/state", ESP.getChipId());
    snprintf(LOG_TOPIC, 128, PROJECT_NAME "/%08X/log", ESP.getChipId());

    snprintf(INSTANCE_NAME, 16, "%08X", ESP.getChipId());

    connectWifi();
#ifdef OTA_SERVER
    doHTTPUpdate();
#endif
    connectMQTT();
}


void loop() {
    connectMQTT();
    checkButton();
    checkState();
}
