Sonoff controller
=================

A Sonoff is [this thing](http://wiki.iteadstudio.com/Sonoff), it's basically a
a wifi-enabled plug, with the appealing property that it runs on an ESP8266
microcontroller, making it much less hackable (in a "honey, there's
a suspicious van parked across the street" sense, not in a "I just electrocuted
myself" sense), which is great.

I didn't like the default firmware because it routes all requests through
a server in China (for all I know), or, even worse, the US. Since there's no way
to disable that behaviour that I found after not searching at all, I decided to
write the ten lines it took to make my own firmware.

Luckily, [arendst has done most of the
work](https://github.com/arendst/Sonoff-MQTT-OTA), and his seems like an
excellent project, but I open-source most of my things, so here it is. Do
whatever you want with this code (MIT license). I don't imagine it will be very
useful to you, but feel free to open pull requests or cans of beer or anything
else you want.

It includes:

* WiFi configuration.
* Customizable initial state.
* MQTT commands.
* MQTT status reporting.
* OTA updates through an [espota server](https://gitlab.com/stavros/espota-server).


Installation
------------

To install, just build the firmware and copy it to your device. It will become
a WiFi access point and ask you to specify a WiFi network to connect to, the
network password, the initial state of the relay (whether it should be on or off
when power comes back on) and the MQTT server hostname.

After that, you're good to go! Logging events will be sent to the `sonoff/<MAC
address>/log` topic, state will be sent to `sonoff/<MAC address>/state`, and you
can send your commands to `sonoff/<MAC address>/command`.

Valid commands are:

* power/relay on/off/toggle
* status
* reboot

Additionally, all sonoffs with this firmware will listen on a special topic,
`sonoff/all/command/ for collective commands, which will allow you to turn
on/off/reboot all sonoffs on the network.

-- Stavros
